export const GET_LOGIN_REQ="GET_LOGIN_REQ";
export const GET_LOGIN_SUC="GET_LOGIN_SUC";
export const GET_LOGIN_ERR="GET_LOGIN_ERR";

export const GET_TOKEN_REQ="GET_TOKEN_REQ";
export const GET_TOKEN_SUC="GET_TOKEN_SUC";
export const GET_TOKEN_ERR="GET_TOKEN_ERR";

export const LOGOUT_REQ="LOGOUT_REQ";