import {
  GET_LOGIN_ERR,
  GET_LOGIN_REQ,
  GET_LOGIN_SUC,
  GET_TOKEN_ERR,
  GET_TOKEN_REQ,
  GET_TOKEN_SUC,
  LOGOUT_REQ,
} from "./ActionType";

const initialState = {
  isLoading: null,
  isUserLoading: false,
  isAuth: localStorage.getItem("access token") ? true : false,
  requestToken: "",
  isErr: false,
  apiKey: "019085ae8fd360fcd800ae1d44592de2",
};

export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LOGIN_REQ:
      return {
        ...state,
        isUserLoading: true,
        isErr: false,
      };
    case GET_LOGIN_SUC:
      return {
        ...state,
        isAuth: true,
        isUserLoading: false,
        isErr: false,
      };
    case GET_LOGIN_ERR:
      return {
        ...state,
        isErr: true,
        isUserLoading: false,
      };

    //Token REQUEST PROCESS
    case GET_TOKEN_REQ:
      return {
        ...state,
        isLoading: true,
        isErr: false,
      };
    case GET_TOKEN_SUC:
      return {
        ...state,
        isAuth: false,
        requestToken: action.payload,
        isErr: false,
        isLoading: false,
      };
    case GET_TOKEN_ERR:
      return {
        ...state,
        isErr: true,
        isLoading: false,
      };
    case LOGOUT_REQ:
      return {
        ...state,
        isLoading: null,
        isUserLoading: false,
        isAuth:false,
        requestToken: "",
        isErr: false,
        apiKey: "019085ae8fd360fcd800ae1d44592de2",
      };

    default:
      return state;
  }
};
