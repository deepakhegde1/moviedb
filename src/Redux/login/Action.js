import axios from "axios";
import { showNotification } from "../../navbar/Notification";
import {
  GET_LOGIN_ERR,
  GET_LOGIN_REQ,
  GET_LOGIN_SUC,
  GET_TOKEN_ERR,
  GET_TOKEN_REQ,
  GET_TOKEN_SUC,
  LOGOUT_REQ,
} from "./ActionType";

export const loginRequest = () => {
  return {
    type: GET_LOGIN_REQ,
  };
};
export const loginSuccess = (payload) => {
  return {
    type: GET_LOGIN_SUC,
    payload,
  };
};
export const loginFail = () => {
  return {
    type: GET_LOGIN_ERR,
  };
};

export const loginProcess =(payload) =>async(dispatch) => {
  dispatch(loginRequest());
  try {
    const temp= await axios.post(`https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=019085ae8fd360fcd800ae1d44592de2`,payload)
    if(temp?.data?.success===true){
       dispatch(loginSuccess());
       localStorage.setItem("access token",temp?.data?.success);
       showNotification("success", "Login success")
    }
    
  } catch (error) {
    dispatch(loginFail());
        showNotification("error","Wrong Credentials")
    
  }
   
  }

// **********************************************************
//   Request Token Call
// **********************************************************

export const tokenRequest = () => {
  return {
    type: GET_TOKEN_REQ,
  };
};
export const tokenReqSuccess = (payload) => {
  return {
    type: GET_TOKEN_SUC,
    payload:payload,
  };
};
export const tokenReqFail = () => {
  return {
    type: GET_TOKEN_ERR,
  };
};
export const tokenRequestProcess =() => async (dispatch) => {
    dispatch(tokenRequest());
   const temp=await axios.get(`https://api.themoviedb.org/3/authentication/token/new?api_key=019085ae8fd360fcd800ae1d44592de2`)
      if(temp?.data?.success===true){
        dispatch(tokenReqSuccess(temp?.data?.request_token))
      }
      else{
        dispatch(tokenReqFail())
      }
  };

  export const logoutRequest=()=>{
    return {
      type: LOGOUT_REQ,
    };
  }
  export const logoutProcess=()=>(dispatch)=>{
   dispatch(logoutRequest())
   showNotification("success", "Logout success")
  }