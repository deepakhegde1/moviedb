import axios from "axios"
import { GET_MOVIELIST_ERR, GET_MOVIELIST_REQ, GET_MOVIELIST_SUC } from "./ActionType"

export const movielistRequest=()=>{
    return{
        type:GET_MOVIELIST_REQ
    }
}
export const movielistSuccess=(payload)=>{
    return{
        type:GET_MOVIELIST_SUC,
        payload,
    }
}
export const movielistFail=()=>{
    return{
        type:GET_MOVIELIST_ERR
    }
}

export const movielistGetProcess=({page,term})=>(dispatch)=>{
  if(page && term===""){
    dispatch(movielistRequest());
  return axios.get(`https://api.themoviedb.org/3/trending/movie/day?api_key=d0605f7c77a7e9ffd22f6f77c12e0f8f&page=${page}`)
  .then(res => {
    const timer = setTimeout(() => {
        dispatch(movielistSuccess(res.data))
      }, 2000);
      return () => clearTimeout(timer);
     
  })
  .catch(err=> {
      dispatch( movielistFail() )
  })

  }
  else if(page&&term){
    dispatch(movielistRequest());
    return axios.get(`https://api.themoviedb.org/3/search/movie?api_key=d0605f7c77a7e9ffd22f6f77c12e0f8f&language=en-US&query=${term}&page=${page}&include_adult=false`)
    .then(res => {
        const timer = setTimeout(() => {
            dispatch(movielistSuccess(res.data))
          }, 2000);
          return () => clearTimeout(timer);
    })
    .catch(err=> {
        dispatch( movielistFail() )
    })

  }
  
}
