import { GET_MOVIELIST_ERR, GET_MOVIELIST_REQ, GET_MOVIELIST_SUC } from "./ActionType"

const initialState={
   isLoading:true,
   data:[],
   isErr:false
}

export const movielistReducer=(state=initialState,{type,payload})=>{
    switch(type){
        case GET_MOVIELIST_REQ:
            return{
                ...state, 
                isLoading:true
            }
        case GET_MOVIELIST_SUC:
            return{
                ...state,
                data:payload,
                isLoading:false

            }
        case GET_MOVIELIST_ERR:
            return{
                ...state,
                isErr:true,
                isLoading:false
            }
        default:
            return state
    }

}