import { applyMiddleware, combineReducers, createStore } from "redux";
import { compose } from "redux";
import thunk from "redux-thunk";
import { loginReducer } from "./login/Reducer";
import { movielistReducer } from "./movielist/Reducer";

const rootreducer = combineReducers({
  movieList: movielistReducer,
  userDetails: loginReducer,
});

const store = createStore(
  rootreducer,
  compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()
  )
);

export default store;
