import { Spin } from 'antd'
import React from 'react'

function Loader() {
  return (
    <div className='spinner'> 

    <Spin size="large"></Spin>
    <p>Please wait Loading...</p>
    
    </div>)
}

export default Loader