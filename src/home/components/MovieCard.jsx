import React, { useState } from "react";
import "../../assets/CSS/Navbar.css";
import ReactStars from "react-stars";
import play from "../../assets/Images/play.png"
import { useNavigate } from "react-router-dom";

function MovieCard({ item,handleMovieDetails }) {
  const navigate=useNavigate();

  const handleNavigation=()=>{
    navigate(`/movie/${item.id}`)
  }

  return (
    <div className="card" onClick={handleNavigation}>
      <div
        className="home-movie-poster"
        style={{
          backgroundImage: `url(https://image.tmdb.org/t/p/w500${item?.poster_path})`,
          backgroundRepeat: "no-repeat",
          borderTopLeftRadius:"5px",
          borderTopRightRadius:"5px",
          backgroundSize:"100% 100%",
          objectFit:"cover"
        }}
      ></div>
      <div className="card2">
        <div className="overflow-title">
          <p className="t3 overflow-title">{item.title}</p>
        <div className="flex">
        <ReactStars
            className="stars"
            value={item?.vote_average / 2}
            count={5}
            edit={false}
            size={24}
            color2={"#ffd700"}
          />
          <span>{(item?.vote_average / 2).toFixed(1)}/5</span>
        </div>
        </div>
        <div>
            <img className="play" src={play} alt="" />
        </div>
      </div>
    </div>
  );
}

export default MovieCard;
