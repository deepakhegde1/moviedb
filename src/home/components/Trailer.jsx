import { Button } from "antd";
import React from "react";
import CustomComponents from "./CustomComponents";
import "../../assets/CSS/Login.css"

function Trailer({visible,setVisible,videoDetails}) {
  return (
    <CustomComponents
    visible={visible}
    className="custom-Model"
      footer={
        <>
          <Button className="closebtn" onClick={() => setVisible(false)}>Close</Button>
        </>
      }
    >
      <iframe
        width="100%"
        height="100%"
        src={`https://www.youtube.com/embed/${videoDetails.key}`}
        title="YouTube video player"
        frameborder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      ></iframe>
    </CustomComponents>
  );
}

export default Trailer;
