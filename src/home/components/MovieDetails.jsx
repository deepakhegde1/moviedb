import React, { useEffect, useState } from "react";
import Arrow from "../../assets/Images/Arrow.png";
import play from "../../assets/Images/play.png";
import "../../assets/CSS/Navbar.css";
import ReactStars from "react-stars";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import Loader from "./Loader";
import Trailer from "./Trailer";

function MovieDetails({ setSearchVisible }) {
  const navigate = useNavigate();
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [selected, setSelected] = useState({});
  const [videoDetails, setVideoDetails] = useState([]);
  const [visible, setVisible] = useState(false);

  const getMovieDetails = async () => {
    const temp = await axios.get(
      `https://api.themoviedb.org/3/movie/${id}?api_key=d0605f7c77a7e9ffd22f6f77c12e0f8f&language=en-US`
    );
    setSelected(temp?.data);
    setIsLoading(false);
  };
  const getMovieVideoDetails = async () => {
    const temp = await axios.get(
      `https://api.themoviedb.org/3/movie/${id}/videos?api_key=d0605f7c77a7e9ffd22f6f77c12e0f8f&language=en-US`
    );
    const trailer_temp = (temp?.data?.results).filter(
      (item) => item.type === "Trailer" 
    );
    setVideoDetails(trailer_temp[0]);
  };
  useEffect(() => {
    setSearchVisible(false);
    const timer = setTimeout(() => {
      getMovieDetails();
      getMovieVideoDetails();
    }, 2000);
    return () => clearTimeout(timer);
  }, [id]);
  return isLoading ? (
    <Loader />
  ) : (
    <div className="movieDetails-container">
      <div className="nd-div">
        <img
          onClick={() => navigate("/movielist")}
          className="nd"
          src={Arrow}
          alt=""
        />
      </div>
      <div
        className="image-container"
        style={{
          backgroundImage: `url(https://image.tmdb.org/t/p/w500${selected?.backdrop_path})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "100% 100%",
        }}
      >
          <img
            onClick={() => setVisible(true)}
            className="playbtn"
            src={play}
            alt=""
          />
      </div>
      <div className="details-container">
        <div>
          <img
            onClick={() => navigate("/movielist")}
            className="arrow"
            src={Arrow}
            alt=""
          />
        </div>
        <div className="movie-title">
          <h1 className="mvie-title">{selected?.title}</h1>
          <div className="flex">
            <ReactStars
              className="stars"
              value={selected?.vote_average / 2}
              count={5}
              size={15}
              edit={false}
              color2={"#ffd700"}
            />
            <span>{(selected?.vote_average / 2).toFixed(1)}/5</span>
          </div>
        </div>
        <div className="movieDetails">
          <p>{selected?.overview}</p>
        </div>
        <div className="movieDetails">
          <p>Release Date: {selected?.release_date}</p>
        </div>
      </div>
        <Trailer visible={visible} setVisible={setVisible} videoDetails={videoDetails}/>
    </div>
  );
}

export default MovieDetails;
