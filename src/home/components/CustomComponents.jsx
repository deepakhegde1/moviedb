import { Modal } from 'antd'
import React from 'react'

function CustomComponents(props) {
  return (
    <Modal
      className={` ${props.className}`}
      style={props.style}
      centered
      visible={props?.visible}
      closable={false}
      cancelText={props.cancelButtonText || "Close"}
      maskClosable={false}
      confirmLoading={props.loading}
      footer={props.footer}
      width={props.width || "75%"}
      bodyStyle={{height:"75vh",background:"#263F61"}}
    >
      {props.children}
    </Modal>
  )
}

export default CustomComponents