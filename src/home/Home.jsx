import { Empty, Spin } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { movielistGetProcess } from "../Redux/movielist/Action";
import { Pagination } from "antd";
import HomeAds from "./components/HomeAds";
import "../assets/CSS/Navbar.css";
import MovieCard from "./components/MovieCard";
import Loader from "./components/Loader";
import { useNavigate } from "react-router-dom";

function Home({ query, setSearchVisible }) {
  const dispatch = useDispatch();
  const navigate=useNavigate();
  const token=localStorage.getItem('access token')
  const movieData = useSelector((state) => state?.movieList);
  const userDetails=useSelector(state=>state?.userDetails)
  const [page, setPage] = useState(movieData?.data?.page || 1);
  
  useEffect(() => {
    if(userDetails.isAuth===false){
      navigate("/");
    }
    else{
      setSearchVisible(true);
      dispatch(movielistGetProcess({ page: page, term: query }));
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    }
 
  }, [page]);
  return (
    <div style={{ width: "100%" }}>
      <div className="Ads-container">
        <HomeAds />
      </div>
      <div className="Ads-container">
        <h2 className="t2">Trending</h2>
      </div>
      <div className="home-card-container">
        {!movieData.isLoading ? (
          movieData?.data?.results?.length === 0 ? (
            <div className="empty">
              <Empty />
            </div>
          ) : (
            (movieData?.data?.results).map((item) => (
              <MovieCard key={item.id} item={item} />
            ))
          )
        ) : (
          <Loader />
        )}
      </div>
   {movieData?.data?.results?.length !== 0 &&   <Pagination
        pageSize={20}
        showTitle={false}
        defaultCurrent={page}
        current={page}
        onChange={(e) => setPage(e)}
        size="large"
        total={movieData?.data?.total_results}
        showSizeChanger={false}
        showQuickJumper={false}
      />}
      <div className="Ads-container"></div>
      <div className="Ads-container"></div>
    </div>
  );
}

export default Home;
