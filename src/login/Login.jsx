import { Button, Input, Row } from "antd";
import React, { useCallback,useEffect,useLayoutEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import "../assets/CSS/Login.css";
import {loginProcess, tokenRequestProcess } from "../Redux/login/Action";

function Login() {
  const navigate = useNavigate();
  const token=localStorage.getItem("access token");
  const dispatch=useDispatch();
  const userDetails=useSelector(state=>state?.userDetails)
  const [userName, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [err, handleError] = useState({ userName: null, password: null });

  if(token === true ){
    navigate("/movielist")
   }

  const handleLogin = useCallback(async () => {
    if (userName && password) {
        const payload={
            username:userName,
            password,
            request_token:userDetails?.requestToken
        }
        dispatch(loginProcess(payload))
    } else {
      let userNameError = null;
      let passwordError = null;
      if (userName?.trim() === "") {
        userNameError = "Username cannot be blank";
      }
      if (password?.trim() === "") {
        passwordError = "Password cannot be blank";
      } else if (password.length < 4) {
        passwordError = "Password length should be 4";
      }
      handleError({ userName: userNameError, password: passwordError });
    }
  }, [userName, password]);

  useEffect(() => {
   if(token === "true" || userDetails?.isAuth===true){
    navigate("/movielist")
   }
   else{
    dispatch(tokenRequestProcess())
   }
  },[(userDetails.isAuth),token]);

  return (
    <div className="sign-in-container">
      <div className="title-container">
        <div className="hh-container">
          <p className="hh1">Sign in</p>
          <p className="hh2">Sign in to your Self Service Portal</p>
        </div>
        <div className="input-container">
          <Row className="in-row">
            <Input
              value={userName}
              onChange={(e) => {
                setUsername(e.target.value);
                handleError({ ...err, userName: null });
              }}
              type="text"
              placeholder="User Name"
            />
            <Row className="error">{err.userName}</Row>
          </Row>

          <Row className="in-row">
            <Input 
              value={password}
              onChange={(e) => {
                setPassword(e.target.value);
                handleError({ ...err, password: null });
              }}
              placeholder="Password" type="password" />
            <Row className="error">{err.password}</Row>
          </Row>
          <Button onClick={handleLogin} className="ant-btn">
            Login
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Login;
