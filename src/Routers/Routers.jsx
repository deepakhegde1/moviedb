import React, { useState } from "react";
import { Route, Routes } from "react-router-dom";
import MovieDetails from "../home/components/MovieDetails";
import Home from "../home/Home";
import Login from "../login/Login";
import Navbar from "../navbar/Navbar";

function Routers() {
  const [query, setQuery] = useState("");
  const [searchVisible, setSearchVisible] = useState(true);

  return (
    <>
      <Navbar query={query} setQuery={setQuery} searchVisible={searchVisible} />
      <Routes>
        <Route path="/" element={<Login/>} />
        <Route
          path="/movielist"
          element={<Home query={query} setSearchVisible={setSearchVisible} />}
        />
        <Route
          path="/movie/:id"
          element={<MovieDetails setSearchVisible={setSearchVisible} />}
        />
      </Routes>
    </>
  );
}

export default Routers;
