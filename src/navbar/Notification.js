import { notification } from "antd";

export const showNotification = (type, msg) => {
    notification[type]({
      message: msg,
      placement: "bottomLeft",
    });
  };