import search from "../assets/Images/search.png";
import React from "react";
import "../assets/CSS/Navbar.css";
import logo from "../assets/Images/logo.png";
import { useDispatch, useSelector } from "react-redux";
import { movielistGetProcess } from "../Redux/movielist/Action";
import { Button } from "antd";
import { useNavigate } from "react-router-dom";
import { logoutProcess } from "../Redux/login/Action";

function Navbar({query,setQuery,searchVisible}) {
  const dispatch = useDispatch();
  const navigate=useNavigate();
  const userDetails=useSelector(state=>state?.userDetails)

  const handleChange=(e)=>{
    setQuery(e.target.value);
    dispatch(movielistGetProcess({page:1,term:e.target.value}))
  }
  const handleLogout=async()=>{
    await localStorage.removeItem('access token');
    dispatch(logoutProcess())
    navigate("/")
  }

  return (
    <div className="navContainer">
      <div className="logoContainer1">
      <div className="logo-Container">
        <img className="logo" src={logo} alt="" />
        { userDetails.isAuth && <div className="btnNd">
          <button className="logoutbtn" onClick={handleLogout}>Logout</button>
        </div>}
      </div>
      </div>
      <div className="search-Container1">
      { searchVisible && (userDetails.isAuth===true)&&<div className="search-Container">
          <button className="button">
            <input
            className="search-input"
              value={query}
              onChange={handleChange}
              placeholder="Search movies"
            />
            <img src={search} alt="" />
          </button>
        </div>}
      </div>
      <div className="logoutbtn-Container">
      { userDetails.isAuth && <div className="">
          <button className="logoutbtn" onClick={handleLogout}>Logout</button>
        </div>}
      </div>
     
    </div>
  );
}

export default Navbar;
